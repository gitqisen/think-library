<?php

namespace musp\admin\entity;


use think\App;
use think\Container;

class AdminAccount
{

    /**
     * @var string 用户标识
     */
    private $sub;

    /**
     * @var  int 用户id
     */
    private $user_id;

    /**
     * @var int 角色id
     */
    private $role_id;

    /**
     * @var array 可浏览部门节点id
     */
    private $dept_ids;

    /**
     * @var bool 是否超级管理员
     */
    private $is_admin;

    /**
     * 初始化用户登录信息
     */
    public function __construct($arr)
    {
        foreach ($arr as $v => $k) {
            isset($this->{$v}) ?: $this->{$v} = $k;
        }
    }


    public function getSub()
    {
        return $this->sub;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function getDeptIds()
    {
        return $this->dept_ids;
    }

    public function getRoleId()
    {
        return $this->role_id;
    }

    public function isAdmin()
    {
            return $this->is_admin;
    }

}
