<?php


declare (strict_types=1);

namespace musp\admin\helper;

use musp\admin\Helper;
use think\admin\service\AdminService;
use think\db\BaseQuery;
use think\db\Query;
use think\exception\HttpResponseException;
use think\Model;

/**
 * 列表处理管理器
 * @class PageHelper
 * @package think\admin\helper
 */
class PageHelper extends Helper
{
    /**
     * 逻辑器初始化
     * @param BaseQuery|Model|string $dbQuery
     * @param boolean|integer $page 是否分页或指定分页
     * @param boolean $display 是否渲染模板
     * @param boolean|integer $total 集合分页记录数
     * @param integer $limit 集合每页记录数
     * @param string $template 模板文件名称
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function init($dbQuery, $page = true, bool $display = true, $total = false, int $limit = 0, string $template = ''): array
    {

    }

    /**
     * 组件 Layui.Table 处理
     * @param BaseQuery|Model|string $dbQuery
     * @param string $template
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function layTable($dbQuery, string $template = ''): array
    {

    }

    /**
     * 输出 XSS 过滤处理
     * @param array $items
     */
    private static function xssFilter(array &$items)
    {
        foreach ($items as &$item) if (is_array($item)) {
            static::xssFilter($item);
        } elseif (is_string($item)) {
            $item = htmlspecialchars($item, ENT_QUOTES);
        }
    }

    /**
     * 查询对象数量统计
     * @param BaseQuery|Query $query
     * @param boolean|integer $total
     * @return integer|boolean|string
     * @throws \think\db\exception\DbException
     */
    private static function getCount($query, $total = false)
    {
        if ($total === true || is_numeric($total)) return $total;
        [$query, $options] = [clone $query, $query->getOptions()];
        if (isset($options['order'])) $query->removeOption('order');
        if (empty($options['union'])) return $query->count();
        $table = [$query->buildSql() => '_union_count_'];
        return $query->newQuery()->table($table)->count();
    }

    /**
     * 绑定排序并返回操作对象
     * @param BaseQuery|Model|string $dbQuery
     * @param string $field 指定排序字段
     * @return \think\db\Query
     * @throws \think\db\exception\DbException
     */
    public function autoSortQuery($dbQuery, string $field = 'sort'): Query
    {
        $query = static::buildQuery($dbQuery);
        if ($this->app->request->isPost() && $this->app->request->post('action') === 'sort') {
            AdminService::isLogin() or $this->class->error('请重新登录！');
            if (method_exists($query, 'getTableFields') && in_array($field, $query->getTableFields())) {
                if ($this->app->request->has($pk = $query->getPk() ?: 'id', 'post')) {
                    $map = [$pk => $this->app->request->post($pk, 0)];
                    $data = [$field => intval($this->app->request->post($field, 0))];
                    $query->newQuery()->where($map)->update($data);
                    $this->class->success('列表排序成功！', '');
                }
            }
            $this->class->error('列表排序失败！');
        }
        return $query;
    }
}
