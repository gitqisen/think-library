<?php


declare (strict_types=1);

namespace musp\admin\helper;

use musp\admin\Helper;
use think\db\BaseQuery;
use think\Model;

/**
 * 表单视图管理器
 * @class FormHelper
 * @package musp\admin\helper
 */
class FormHelper extends Helper
{

    /**
     * 数据新增处理器
     * @param BaseQuery|Model|string $dbQuery 数据对象
     * @param string $fields 允许写入的字段
     * @param string $pk 主键字段
     * @param string $exclude 排除的字段
     * @param array $edata 附加数据
     * @return void
     */
    public function init($dbQuery, string $fields = '',string $pk = '',string $exclude = '', array $edata = [])
    {
        $query = static::buildQuery($dbQuery);
        $pk = $pk ?: ($query->getPk() ?: 'id'); // 获取主键
        if ($this->app->request->isPost()) {
            if (empty($fields) && empty($exclude)){
                $fields = $query->withoutField($pk)->getTableFields();
            }else if (!empty($exclude)){
                $fields = $query->withoutField($exclude)->getTableFields();
            }else{
                $fields = explode(',',$fields);
            }
            $data = array_merge($this->app->request->only($fields), $edata);
            toResult($query->save($data));
        }else{
            error('当前请求错误！');
        }
    }
}
