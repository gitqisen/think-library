<?php


declare (strict_types=1);

namespace musp\admin\helper;

use musp\admin\Helper;
use think\db\BaseQuery;
use think\db\exception\DbException;
use think\Model;

/**
 * 数据更新管理器
 * @class SaveHelper
 * @package think\admin\helper
 */
class SaveHelper extends Helper
{

    /**
     * 数据编辑处理器
     * @param BaseQuery|Model|string $dbQuery 数据对象
     * @param string $fields
     * @param string $pk
     * @param array $edata
     * @param string $exclude
     * @param array $where
     * @throws DbException
     */
    public function init($dbQuery, string $fields = '', string $pk = '', array $edata = [], string $exclude = '', array $where = [])
    {
        $query    = static::buildQuery($dbQuery);
        $pk       = $pk ?: ($query->getPk() ?: 'id'); // 获取主键
        $pk_value = $this->app->request->param($pk);
        if (empty($field) && empty($exclude)) { // 获取全部字段 除主键
            $fields = $query->withoutField($pk)->getTableFields();
        } else if (!empty($exclude)) { // 获取排除外的字段 除主键
            $fields = $query->withoutField($exclude . ',' . $pk)->getTableFields();
        } else {
            $fields = explode(',', $fields);
        }
        $data = array_merge($this->app->request->only($fields), $edata);
        if (empty($pk_value) && empty($where)) error('更新条件不能为空！');
        // 回复前端结果
        toResult($query->master()->where(array_merge($where, [[$pk, '=', $pk_value]]))->save($data));
    }
}
