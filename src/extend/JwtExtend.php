<?php

declare (strict_types=1);

namespace musp\admin\extend;

use Exception;

/**
 * 接口 JWT 接口扩展
 * @class JwtExtend
 * @package think\admin\extend
 */
class JwtExtend
{
    // 标识字段
    private const skey = '__ISJWT_SESS__';

    // 头部参数
    private const header = ['typ' => 'JWT', 'alg' => 'HS256'];

    // 签名配置
    private const signTypes = [
        'HS256' => 'sha256',
        'HS384' => 'sha384',
        'HS512' => 'sha512'
    ];

    /**
     * 生成 jwt token
     * @param ?array $payload jwt 载荷 格式如下非必须
     * [
     *     'iss' => 'jwt_admin',               // 该JWT的签发者
     *     'iat' => time(),                    // 签发时间
     *     'exp' => time() + 7200,             // 过期时间
     *     'sub' => '',                        // 面向的用户
     *     'token' => md5(uniqid('JWT').time())  // 该 Token 唯一标识
     * ]
     * @param ?string $jwtkey 签名密钥
     * @return string
     */
    public static function token(?array $payload = null): string
    {
        $base64header = CodeExtend::enSafe64(json_encode(static::header, JSON_UNESCAPED_UNICODE));
        $base64payload = CodeExtend::enSafe64(json_encode($payload, JSON_UNESCAPED_UNICODE));
        $signature = static::withSign($base64header . '.' . $base64payload, static::header['alg'], static::jwtkey());
        return $base64header . '.' . $base64payload . '.' . $signature;
    }

    /**
     * 验证 token 是否有效, 默认验证 exp,nbf,iat 时间
     * @param string $token 加密数据
     * @param ?string $jwtkey 签名密钥
     * @return array
     */
    public static function verify(string $token, ?string $jwtkey = null): array
    {

        $tokens = explode('.', str_replace('Bearer ','',$token));
        if (count($tokens) != 3) error('登录状态过期！',4001);
        [$base64header, $base64payload, $signature] = $tokens;
        // 加密算法
        $header = json_decode(CodeExtend::deSafe64($base64header), true);
        if (empty($header['alg'])) error('登录状态过期！',4001);
        // 签名验证
        if (self::withSign("{$base64header}.{$base64payload}", $header['alg'], static::jwtkey($jwtkey)) !== $signature) {
            error('登录状态过期！',4001);
        }
        // 获取数据
        $payload = json_decode(CodeExtend::deSafe64($base64payload), true);
        // 签发时间大于当前服务器时间验证失败
        if (isset($payload['iat']) && $payload['iat'] > time()) {
            error('登录状态过期！',4001);
        }
        // 过期时间小于当前服务器时间验证失败
        if (isset($payload['exp']) && $payload['exp'] < time()) {
            error('登录状态过期！',4001);
        }
        return $payload;
    }

    /**
     * 获取 JWT 密钥
     * @param ?string $jwtkey
     * @return string
     */
    public static function jwtkey(?string $jwtkey = null): string
    {
        try {
            if (!empty($jwtkey)) return $jwtkey;

            // 优先读取配置文件
            $jwtkey =  env('app.jwt_key');
            if (!empty($jwtkey)) return $jwtkey;
            // 自动生成新的密钥
            $jwtkey = md5(uniqid(strval(rand(1000, 9999)), true));
            sysconf('data.jwtkey', $jwtkey);
            return $jwtkey;

        } catch (\Exception $exception) {
            trace_file($exception);
            return 'thinkadmin';
        }
    }

    /**
     * 设置输出数据并切换模式
     * @param array $data
     * @return array
     */
    public static function setOutData(array $data = []): array
    {
        self::$isjwt = true;
        return static::$outData = $data;
    }

    /**
     * 是否返回令牌
     * @return boolean
     */
    public static function isRejwt(): bool
    {
        return self::$rejwt;
    }


    /**
     * 生成数据签名
     * @param string $input 为 base64UrlEncode(header).".".base64UrlEncode(payload)
     * @param string $alg 算法方式
     * @param ?string $key 签名密钥
     * @return string
     */
    private static function withSign(string $input, string $alg = 'HS256', ?string $key = null): string
    {
        return CodeExtend::enSafe64(hash_hmac(self::signTypes[$alg], $input, static::jwtkey($key), true));
    }


}
