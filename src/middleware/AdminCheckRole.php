<?php
namespace musp\admin\middleware;
use app\Request;
use Closure;
use musp\admin\service\auth\AdminAuthService;

/**
 * 权限节点鉴定中间件
 * Class AdminCheckRole
 * @package app\admin\middleware
 */
class AdminCheckRole
{
    public function handle(Request $request, Closure $next)
    {
        // 超级管理员不用判断节点权限
        if (app()->account->isAdmin()) return $next($request);
        AdminAuthService::instance()->checkPermission();
        return $next($request);
    }

}

