<?php
namespace musp\admin\middleware;
use app\Request;
use Closure;
use musp\admin\service\auth\AdminAuthService;

/**
 * 登录状态检测中间件
 * Class AdminCheckToken
 * @package app\admin\middleware
 */
class AdminCheckToken
{
    public function handle(Request $request, Closure $next)
    {
        $token = $request->header('Authorization','Bearer ');
        AdminAuthService::instance()->verifyToken($token);
        return $next($request);
    }
}
