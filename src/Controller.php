<?php

namespace musp\admin;

use musp\admin\entity\AdminAccount;
use stdClass;
use think\App;
use think\Request;
use musp\admin\helper\ValidateHelper;

/**
 * 请求基础入口
 * Class Controller
 * @package musp\admin
 */
class Controller extends stdClass
{


    /**
     * 应用容器
     * @var App
     */
    public $app;

    /**
     * 请求GET参数
     * @var array
     */
    public $get = [];

    /**
     * 当前功能节点
     * @var string
     */
    public $node;

    /**
     * 请求参数对象
     * @var Request
     */
    public $request;

    public $ua;

    /**
     * Constructor.
     * @param App $app
     */
    public function __construct(App $app)
    {
        if (in_array($app->request->action(), get_class_methods(__CLASS__))) {
            error('禁止访问内置方法！', 403);
        }
        $this->get     = $app->request->get();
        $this->app     = $app;
        $this->request = $app->request;
        $this->app->account = null;
        $this->initialize();
    }

    /**
     * 控制器初始化
     */
    protected function initialize()
    {

    }


    /**
     * 快捷输入并验证（ 支持 规则 # 别名 ）
     * @param array $rules 验证规则（ 验证信息数组 ）
     * @param string|array $type 输入方式 ( post. 或 get. )
     * @param callable|null $callable 异常处理操作
     * @return array
     */
    protected function _vali(array $rules, $type = '', ?callable $callable = null): array
    {
        return ValidateHelper::instance()->init($rules, $type, $callable);
    }
}
