<?php


namespace musp\admin\service;


use musp\admin\helper\DeleteHelper;
use musp\admin\helper\FormHelper;
use musp\admin\helper\QueryHelper;
use musp\admin\helper\SaveHelper;
use think\Container;
use think\Model;

/**
 * 接口统一处理模型
 * Class ApiModel
 * @package musp\admin\service
 *
 *
 *  静态助手调用
 * @method static bool mSave(array $data = [], string $field = '', mixed $where = []) 快捷更新
 * @method static bool mDelete(string $field = '', mixed $where = []) 快捷删除
 * @method static bool|array mForm(string $template = '', string $field = '', mixed $where = [], array $data = []) 快捷表单
 * @method static bool|integer mUpdate(array $data = [], string $field = '', mixed $where = []) 快捷保存
 * @method static QueryHelper mQuery($input = null, callable $callable = null) 快捷查询
 */
class ApiModel extends Model
{

    /**
     * 创建模型实例
     * @template t of static
     * @param mixed $data
     * @return t|static
     */
    public static function mk($data = [])
    {
        return new static($data);
    }

    /**
     * 创建查询实例
     * @param array $data
     * @return \think\db\Query|\think\db\Mongo
     */
    public static function mq(array $data = [])
    {
        return static::mk($data)->newQuery();
    }


    /**
     * 调用魔术方法
     * @param string $method 方法名称
     * @param array $args 调用参数
     * @return $this|false|mixed
     */
    public function __call($method, $args)
    {
        if (isset($oplogs[$method])) {
            return $this;
        } else {
            return parent::__call($method, $args);
        }
    }


    /**
     * 静态魔术方法
     * @param string $method 方法名称
     * @param array $args 调用参数
     * @return mixed|false|integer|QueryHelper
     */
    public static function __callStatic($method, $args)
    {
        $helpers = [
            'mForm'   => [FormHelper::class, 'init'],
            'mSave'   => [SaveHelper::class, 'init'],
            'mQuery'  => [QueryHelper::class, 'init'],
            'mDelete' => [DeleteHelper::class, 'init'],
        ];

        if (isset($helpers[$method])) {
            [$class, $method] = $helpers[$method];
            return Container::getInstance()->invokeClass($class)->$method(static::class, ...$args);
        } else {
            return parent::__callStatic($method, $args);
        }
    }
}
