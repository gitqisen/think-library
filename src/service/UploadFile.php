<?php

namespace musp\admin\service;

use musp\admin\model\SysAlbum;
use musp\admin\Service;
use think\file\UploadedFile;

class UploadFile extends Service
{


    /**
     *
     * Date: 2024/2/24 13:09
     * @param UploadedFile $file
     * @param int $album_group_id 照片分组id
     * @param bool $is_zip 是否压缩
     * @param bool $result 立即返回
     * @return array|void
     */
    public function image(UploadedFile $file, int $album_group_id = 0, bool $is_zip = false, bool $result = true)
    {
        $file_name = $file->getOriginalName();
        $file_size = $file->getSize();
        $file_ext = $file->getOriginalExtension();
        if (!in_array($file_ext, ['jpg', 'gif', 'png', 'bmp', 'jpeg', 'wbmp'])) return error('图片未通过安全检查！', end: $result);
        if ($is_zip) {
            // TODO
        }// 调用图片压缩
        $save_path     = $this->getSavePath('image');
        $new_file_name = uniqid() . createno('upload_file') . '.' . $file_ext;
        $local_path = public_path() . $save_path;
        try {
            $file->move( $local_path, $new_file_name);
        } catch (\Exception $e) {
            return error('上传失败！', end: $result);
        }
        // 获取图片尺寸
        [$width, $height] = getimagesize($local_path . '/' . $new_file_name);
        // 存入本地相册
        SysAlbum::instance()->addAlbum([
            'album_group_id' => $album_group_id,
            'file_name'      => $file_name,
            'file_size'      => $file_size,
            'store_name'     => 'local',
            'spec'           => $width . 'x' . $height,
            'path'           => $save_path . '/' . $new_file_name,
            'is_zip'         => $is_zip,
            'create_time'    => time(),
        ]);
        return success([
            'path' => $save_path . '/' . $new_file_name,
        ], '上传成功！');
    }

    /**
     * 检查图片是否安全
     * @param string $filename
     * @return boolean
     */
    private function imgNotSafe(string $filename): bool
    {
        $source = fopen($filename, 'rb');
        if (($size = filesize($filename)) > 512) {
            $hexs = bin2hex(fread($source, 512));
            fseek($source, $size - 512);
            $hexs .= bin2hex(fread($source, 512));
        } else {
            $hexs = bin2hex(fread($source, $size));
        }
        if (is_resource($source)) fclose($source);
        $bins = hex2bin($hexs);
        /* 匹配十六进制中的 <% ( ) %> 或 <? ( ) ?> 或 <script | /script> */
        foreach (['<?php ', '<% ', '<script '] as $key) if (stripos($bins, $key) !== false) return true;
        return preg_match("/(3c25.*?28.*?29.*?253e)|(3c3f.*?28.*?29.*?3f3e)|(3C534352495054)|(2F5343524950543E)|(3C736372697074)|(2F7363726970743E)/is", $hexs);
    }


    private function getSavePath($pathType = 'common'): array|string
    {
        $path     = 'upload/' . $pathType . '/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
        $path_abs = public_path() . 'upload/' . $pathType . '/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
        if (file_exists($path_abs) || mkdir($path_abs, 0755, true)) {
            return $path;
        }
        return error('上传文件失败，请检查文件夹权限');
    }


    /**
     * 缩略图生成
     */
    public function thumbBatch($file_path, $file_name, $extend_name, $thumb_type = [])
    {

        $thumb_type_array = array(
            "BIG"   => array(
                "size"       => "BIG",
                "width"      => $this->config["thumb"]["thumb_big_width"],
                "height"     => $this->config["thumb"]["thumb_big_height"],
                "thumb_name" => ""
            ),
            "MID"   => array(
                "size"       => "MID",
                "width"      => $this->config["thumb"]["thumb_mid_width"],
                "height"     => $this->config["thumb"]["thumb_mid_height"],
                "thumb_name" => ""
            ),
            "SMALL" => array(
                "size"       => "SMALL",
                "width"      => $this->config["thumb"]["thumb_small_width"],
                "height"     => $this->config["thumb"]["thumb_small_height"],
                "thumb_name" => ""
            )
        );


        foreach ($thumb_type_array as $k => $v) {
            if (!empty($thumb_type) && in_array($k, $thumb_type)) {
                $new_path_name = $file_name . "_" . $v["size"] . "." . $extend_name;
                $result        = $this->imageThumb($file_path, $new_path_name, $v["width"], $v["height"], $v["size"] != 'BIG' ? 'center' : '');
                //返回生成的缩略图路径
                if ($result["code"] >= 0) {
                    $thumb_type_array[$k]["thumb_name"] = $new_path_name;
                } else {
                    return $result;
                }
            }
        }
        return $this->success($thumb_type_array);
    }

}
