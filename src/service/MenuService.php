<?php

declare (strict_types=1);

namespace musp\admin\service;

use musp\admin\extend\DataExtend;
use musp\admin\model\SysMenu;
use musp\admin\model\SysRoleMenu;
use think\trace\Service;

/**
 * 系统菜单管理服务
 * @class MenuService
 * @package app\admin\service
 */
class MenuService extends Service
{
    /**
     * 根据用户角色id获取菜单树
     * Date: 2024/3/19 13:45
     * @param $role_id
     */
    public function getMenuTreeBayUserId($role_id)
    {

        $menu_ids = SysRoleMenu::instance()->getMenuIds($role_id);

        $menu_list = SysMenu::instance()->getMenuList([['menu_id','in',$menu_list]],'');




    }

}
