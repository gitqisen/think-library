<?php


namespace musp\admin\service\auth;


use Exception;
use musp\admin\entity\AdminAccount;
use musp\admin\extend\JwtExtend;
use musp\admin\model\SysAccount;
use musp\admin\model\SysRoleMenu;
use musp\admin\model\SysUser;
use musp\admin\model\SysMenu;

/**
 * 后台鉴权服务
 * Class AdminAuthService
 * @package app\service\auth
 */
class AdminAuthService extends BaseAuthService
{
    /**
     *  通过jwt获取用户信息
     */
    public function verifyToken($jwt)
    {
        try {
            $jwt_body = JwtExtend::verify($jwt);
            $user_id = SysAccount::instance()->getUserId($jwt_body['token'],'admin');
            // 加载用户信息
            $user_info = SysUser::instance()->getUserInfo([['user_id', '=', $user_id]]);
            if (empty($user_info) || $user_info['status'] == 0) return error('用户状态异常', 4002);
            $this->app->account = new AdminAccount([
                'user_id'  => $user_info['user_id'],
                'is_admin' => $user_info['is_admin'],
                'role_id'  => $user_info['role_id'],
            ]);
        } catch (Exception $e) {
            error('令牌过期', 4001);
        }
    }
    /**
     * 权限检测
     */
    public function checkPermission(): void
    {
        $request  = $this->app->request;
        $action   = $request->request()['s'];
        $menu_all = SysMenu::instance()->getMenuApiList([['status', '=', 1]]);
        if (in_array($action, $menu_all)) {
            $menu_ids       = SysRoleMenu::instance()->getMenuIds($this->app->account->getRoleId());
            $role_menu_info = SysRoleMenu::instance()->getMenuApiList([
                ['menu_id', 'in', $menu_ids], ['status', '=', 1]
            ]);
            if (!in_array($action, $role_menu_info)) error('权限不足', 4005);
        }
    }

}
