<?php


namespace musp\admin\service\auth;


use app\Request;
use Closure;
use musp\admin\extend\JwtExtend;
use musp\admin\model\SysAccount;
use musp\admin\model\SysUser;

/**
 * 后台登录
 * Class AdminLogin
 * @package app\service\auth
 */
class AdminLoginService extends BaserLoginService
{

    /**
     * 用户登录
     * Date: 2024/3/19 13:24
     */
    public function accountLogin($params)
    {
        $user = SysUser::instance()->getUserInfo([['user_name', '=', $params['user_name']]]);

        if (empty($user)) return error('当前用户不存在');


        if ($user['password'] != md5(md5($params['password']) . env('app.password_key'))) return error('密码错误');

        if ($user['status'] != 1) return error('当前用户状态异常');

        $token = $this->createToken($user);

        return success(['token' => $token]);

    }


    /**
     * 创建登录令牌
     * @param array $user 用户信息
     */
    public function createToken($user)
    {

        SysUser::instance()->updateUser([
                'login_date' => time(),
                'login_ip'   => request()->ip(),
            ]
            , [['user_id', '=', $user['user_id']]]);

        $token = md5(uniqid('JWT') . time());
        $sub   = $this->app->request->header('User-Agent');
        $iat   = time();
        $exp   = time() + 3600 * 24 * 360;
        $jwt   = JwtExtend::token([
            'token' => $token,
            'iss'   => 'admin',
            'exp'   => $exp,
            'iat'   => $iat,
            'sub'   => $sub
        ]);

        // 写入token到持久化层
        SysAccount::instance()->addAccount([
            'token'    => $token,
            'user_id'  => $user['user_id'],
            'channel'  => 'admin',
            'iat'      => $iat,
            'exp'      => $exp,
            'sub'      => $sub,
            'login_ip' => request()->ip(),
        ]);

        return 'Bearer ' . $jwt;

    }


}
