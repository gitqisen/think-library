<?php


namespace musp\admin\service\auth;


use think\App;
use think\Container;
use think\trace\Service;

/**
 * 基础鉴权服务
 * Class BaseAuthService
 * @package app\service\auth
 */
abstract class BaseAuthService extends Service
{


    /**
     * 初始化鉴权部分
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * 初始化服务
     */
    protected function initialize()
    {
    }

    /**
     * 静态实例对象
     * @param array $var 实例参数
     * @param boolean $new 创建新实例
     * @return static|mixed
     */
    public static function instance(array $var = [], bool $new = false)
    {
        return Container::getInstance()->make(static::class, $var, $new);
    }





}
