<?php

namespace musp\admin\model;


/**
 * 系统用户部门服务
 * Class PostService
 * @package musp\admin\service
 */
class SysPost extends SysBaseModel
{

    public function getPostPageList($condition, $page, $page_size, $field = "*", $join = [], $order = '', $alias = 'a')
    {
        return cacheMobile("sys_post")->pageList($condition, $field, $order, $page, $page_size, $alias, $join);
    }

    public function addPost($data)
    {
        return cacheMobile("sys_post")->add($data);
    }


    public function updatePost($data, $where)
    {
        return cacheMobile("sys_post")->update($data, $where);
    }

    public function deletePost($where)
    {

    }




}
