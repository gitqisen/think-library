<?php


namespace musp\admin\model;

use think\helper\Str;


/**
 * 系统菜单
 * Class SysMenu
 * @package musp\admin\model
 */
class SysMenu extends SysBaseModel
{

    public function getMenuList($where)
    {
        return cacheMobile('sys_menu')->getList($where);
    }


    public function getMenuApiList($where)
    {
        $where[] = ['api_url', '<>', ''];
        return cacheMobile('sys_menu')->getColumn($where, 'api_url');
    }

    /**
     * 获取菜单并一树形结构返回
     * Date: 2024/3/19 13:59
     * @param $where
     * @param $field
     */
    public function getMenuTree($where = [], $field = 'menu_id,name,title,parent_id,path,component,redirect,is_iframe,is_keep_alive,menu_type,is_affix,is_link,is_hide,roles,icon')
    {
        $list = cacheMobile("sys_menu")->getList($where, $field, 'sort desc');
        return $this->menuToTree($list);
    }

    /**
     * 把返回的数据集转换成Tree
     * @param array $list 要转换的数据集
     * @return array
     */
    public function menuToTree($list, $root = 0, $pk = 'menu_id', $pid = 'parent_id', $child = 'children')
    {
        $list = $this->snakeMenu($list);
        $tree = [];
        if (!is_array($list)) return [];
        $refer = [];
        foreach ($list as $key => $data) {
            $refer[$data[$pk]] =& $list[$key];
        }
        foreach ($list as $key => $data) {
            $parent_id = $data[$pid];
            if ($root == $parent_id) {
                $tree[] =& $list[$key];
            } else {
                if (isset($refer[$parent_id])) {
                    $parent           =& $refer[$parent_id];
                    $parent[$child][] =& $list[$key];
                }
            }
        }
        return $tree;
    }


    /**
     * 转换驼峰以及其他参数
     * Date: 2024/3/21 15:54
     * @param $data
     * @return array
     */
    private function snakeMenu($array): array
    {
        foreach ($array as $k => $t) {

            $array[$k] = [
                'menu_id'   => $t['menu_id'],
                'path'      => $t['path'],
                'name'      => $t['name'],
                'component' => $t['component'],
                'parent_id' => $t['parent_id'],
                'menu_type' => $t['menu_type'],
                'meta'      => [
                    'title'         => $t['title'],
                    'is_link'       => $t['is_link'],
                    'is_hide'       => $t['is_hide'],
                    'is_keep_alive' => $t['is_keep_alive'],
                    'is_affix'      => $t['is_affix'],
                    'is_iframe'     => $t['is_iframe'],
                    'roles'         => empty($t['roles']) ? [] : explode(',', $t['roles']),
                    'icon'          => $t['icon'],
                ],
            ];
            if (isset($t['sort'])) $array[$k]['sort'] = $t['sort'];
            if (isset($t['status'])) $array[$k]['status'] = $t['status'];
        }
        return $array;
    }

    public function addMenu($data)
    {
        return cacheMobile("sys_menu")->add($data);
    }

    public function deleteMenu($menu_ids)
    {
        $list = cacheMobile("sys_menu")->delete([['parent_id', 'in', $menu_ids]]);
        if (!empty($list)) return error('当前菜单下存在子菜单，请先删除子菜单');
        return cacheMobile("sys_menu")->delete([['menu_id', 'in', $menu_ids]]);
    }


    public function update($data, $where)
    {
        return cacheMobile("sys_menu")->update($data, $where);
    }


}
