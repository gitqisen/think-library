<?php


namespace musp\admin\model;


/**
 * 系统字典
 * Class SysDict
 * @package musp\admin\model
 */
class SysDict extends SysBaseModel
{

    public function getDictPageList($condition, $page, $page_size, $field = "*", $join = [], $order = '', $alias = 'a')
    {
        $res = cacheMobile("sys_dict")->pageList($condition, $field, $order, $page, $page_size, $alias, $join);
        return success($res);
    }


    public function addDict($data)
    {
        // 查询当前配置键是否存在
        $dict_id = cacheMobile('sys_dict')->getColumn([['dict_key','=',$data['dict_key0']]],'dict_id');
        if ($dict_id) {
            return error(-1, '配置键已存在');
        }
        $data['create_time'] = time();
        return cacheMobile('sys_dict')->add($data);
    }


    public function edit($data,$dict_type)
    {
        $data['update_time'] = time();
        return cacheMobile('sys_dict')->update($data,[['dict_type','=',$dict_type]]);
    }


    public function delete($dict_type)
    {
        cacheMobile('sys_dict_data')->delete([['dict_type','=',$dict_type]]);
        cacheMobile('sys_dict_data')->delete([['dict_type','=',$dict_type]]);
        return true;
    }




}
