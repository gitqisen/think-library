<?php


namespace musp\admin\model;


/**
 * 部门管理
 * Class SysRole
 * @package musp\admin\model
 */
class SysRole extends  SysBaseModel
{
    public function getDeptMenuId($where)
    {
        return cacheMobile('sys_role_menu')->getColumn($where,'menu_id');
        }
}
