<?php


namespace musp\admin\model;


/**
 * 用户信息表
 * Class SysUser
 * @package app\model\sys
 */
class SysUser extends SysBaseModel
{


    public function getUserInfo($where = [])
    {
        return cacheMobile('sys_user')->getInfo($where);
    }

    public function getUserPageList($condition, $page, $page_size, $field = "*", $join = [], $order = '', $alias = 'a')
    {
        return cacheMobile("sys_user")->pageList($condition, $field, $order, $page, $page_size, $alias, $join);
    }

    public function updateUser($data, $where)
    {
        return cacheMobile('sys_user')->update($data, $where);
    }

    public function addUser($data)
    {
        $data['create_time'] = time();
        return cacheMobile('sys_user')->add($data);
    }


    public function delUser($where){
        return cacheMobile('sys_user')->delete($where);
    }

}
