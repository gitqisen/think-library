<?php

namespace musp\admin\model;


class SysAlbum extends SysBaseModel
{

    public function getAlbumPageList($condition, $page, $page_size, $field = "*", $join = [], $order = '', $alias = 'a')
    {
        $res = cacheMobile("sys_album")->pageList($condition, $field, $order, $page, $page_size, $alias, $join);
        return success($res);
    }


    public function updateAlbum($data, $where)
    {
        return cacheMobile("sys_album")->update($data, $where);
    }


    public function addAlbum($data)
    {
        return cacheMobile("sys_album")->add($data);
    }

    public function deleteAlbum($where)
    {
        // 触发删除本地目标文件
        $list = cacheMobile("sys_album")->getList($where,'path');
        foreach ($list as $k=>$v){
            if (file_exists($v['path'])){
                unlink(public_path() . $v['path']);
            }
        }
        return cacheMobile("sys_album")->delete($where);
    }

}

