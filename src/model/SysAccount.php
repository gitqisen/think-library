<?php

namespace musp\admin\model;


class SysAccount extends SysBaseModel
{

    public function getAccountPageList($condition, $page, $page_size, $field = "*", $join = [], $order = '', $alias = 'a')
    {
        $res = model("sys_account")->pageList($condition, $field, $order, $page, $page_size, $alias, $join);
        return $this->success($res);
    }

    public function addAccount($data)
    {
        return cacheMobile('sys_account')->add($data);
    }

    public function deleteAccount($where)
    {
        return cacheMobile('sys_account')->delete($where);
    }

    public function getUserId($token, $channel = 'admin')
    {
        return cacheMobile('sys_account')->getValue([['token', '=', $token], ['channel', '=', $channel]], 'user_id');
    }

}
