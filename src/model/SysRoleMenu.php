<?php


namespace musp\admin\model;


/**
 * SysRoleMenu
 * Class SysRoleMenu
 * @package musp\admin\model
 */
class SysRoleMenu extends SysBaseModel
{

    public function getList($where)
    {
        return cacheMobile('sys_role_menu')->getList($where);
    }

    public function addRoleMenu($data)
    {
        return cacheMobile('sys_role_menu')->add($data);
    }

    public function deleteRoleMenu($where)
    {
        return cacheMobile('sys_role_menu')->delete($where);
    }

    /**
     * 根据角色id获取所有管理的菜单id
     * Date: 2024/3/19 13:51
     * @param $role_id
     */
    public function getMenuIds($role_id)
    {
        return cacheMobile('sys_role_menu')->getColumn([['role_id', '=', $role_id]], 'menu_id');
    }


}
