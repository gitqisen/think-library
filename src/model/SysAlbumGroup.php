<?php

namespace musp\admin\model;


class SysAlbumGroup extends SysBaseModel
{

    public function getAlbumGroupList($where, $field = '*')
    {
        return cacheMobile('sys_album_group')->getList($where, $field, 'sort asc,create_time desc');
    }

    public function addAlbumGroup($data)
    {
        $data['create_time'] = time();
        return cacheMobile('sys_album_group')->add($data);
    }

    public function updateAlbumGroup($data, $where)
    {
        return cacheMobile('sys_album_group')->update($data, $where);
    }

    public function deleteAlbumGroup($album_group_id)
    {
        $count_album = cacheMobile('sys_album')->getCount([['album_group_id', 'in', $album_group_id]]);
        if ($count_album > 0) return error('当前相册组下有相册，无法删除');
        return cacheMobile('sys_album_group')->delete([['album_group_id', 'in', $album_group_id]]);
    }

    public function getAlbumGroupTree($where, $field = '*')
    {
        $list = cacheMobile('sys_album_group')->getList($where, $field, 'sort asc,create_time desc');
        return listToTree($list, 'album_group_id', 'group_pid');
    }
}
