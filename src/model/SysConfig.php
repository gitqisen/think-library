<?php

namespace musp\admin\model;


use think\App;
use think\Container;

class SysConfig extends SysBaseModel
{


    public function getConfigPageList($condition, $page, $page_size, $field = "*", $join = [], $order = '', $alias = 'a')
    {
        $res = cacheMobile("sys_config")->pageList($condition, $field, $order, $page, $page_size, $alias, $join);
        return success($res);
    }


    public function getConfigValue($key, $default)
    {
        return cacheMobile('sys_config')
            ->beforeCache(function ($result) use ($default) {
                if (empty($result)) return $default;
                switch ($result['config_value_type']) {
                    case 'int':
                        return intval($result['config_value']);
                    case 'float':
                        return floatval($result['config_value']);
                    case 'bool':
                        return boolval($result['config_value']);
                    case 'json':
                        return json_decode($result['config_value'], true);
                    default:
                        return $result['config_value'];
                }
            })
            ->getInfo([['config_key', '=', $key]], 'config_key,config_value,config_value_type');
    }

    public function setConfig($key, $name, $value, $value_type = 'string', $type = 0, $remark = '')
    {
        // 先判断当前参数是否存在
        $config_id = cacheMobile('sys_config')->getValue([['config_key', '=', $key]], 'config_id');

        try {
            if (!empty($config_id)) {
                cacheMobile('sys_config')->update(['config_value' => $value,'update_time'=>time()], [['config_id', '=', $config_id]]);
            }else{
                cacheMobile('sys_config')->add([
                    'config_key' => $key,
                    'config_name' => $name,
                    'config_value' => $value,
                    'config_value_type' => $value_type,
                    'config_type' => $type,
                    'remark' => $remark,
                    'create_time' => time(),
                    'update_time' => time(),
                ]);
            }
        }catch (\Exception $exception){
            error('操作失败',1,false);
        }

        return success([], '操作成功',1,false);
    }


}
